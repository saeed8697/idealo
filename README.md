## Robot case 

simple environment for a robot where you could control it using this
predefined script:
✓ POSITION 1 //sets the initial position for the robot
✓ FORWARD 3 //lets the robot do 3 steps forward
✓ WAIT //lets the robot do nothing
✓ TURNAROUND //lets the robot turn around
✓ FORWARD 1 //lets the robot do 1 step forward
✓ RIGHT //lets the robot turn right
✓ FORWARD 2 //lets the robot do 2 steps forward

This script  sent from frontend to backend as a single chunk using POST
Method. After script execution UI  render a new robot position on the grid and
direction it looks to.

## Getting Started on backend 

The project uses mangoDb as database for handling states and required for running mangodb on local
first of all : 
1. run mongo db on your localhost see more on this link : https://docs.mongodb.com/guides/server/install/
2. run java spring application server/robot/src/main/java/com/sample/robot/RobotApplication.java
running server on localhost:8080 
now your backend is running :)

### Development
Front-end written in next.js 

To run on development :

```bash
npm run dev
# or
yarn dev
```
#for testing with cypress 
npm run cypress

### Production

To compile for production build:

```sh
npm run build
# or
yarn build
```




Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

this is the sample code  developed by Saeed Hasanabadi 













