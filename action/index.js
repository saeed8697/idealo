import axios from "axios";

const Base_Url = "http://localhost:8080/api/v1"

export const SCRIPT = [
    "POSITION-1",
    "WAIT",
    "RIGHT",
    "TURNAROUND",
    "FORWARD-1",
    "FORWARD-2",
    "FORWARD-3"
]


const RIGHT = 0;
const DOWN = 1;
const LEFT = 2;
const UP = 3;

export const getState = () => {
    return axios.get(`${Base_Url}/game/state`).then(res => res.data)
}

export const runscript = (code) => {

    return axios.post(`${Base_Url}/game/run`, code).then(res => res.data)
}





