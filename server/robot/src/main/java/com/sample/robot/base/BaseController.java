package com.sample.robot.base;


import com.sample.robot.pojo.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BaseController {

    private Logger logger = LoggerFactory.getLogger(BaseController.class);


    protected Response invalidRequest(HttpServletResponse response) throws IOException {

        return respondError(400,"Check Your input", response);
    }
    protected Response respondError(int code, String message, HttpServletResponse response) throws IOException {
        response.sendError(code);
        return new Response(false, message, null);
    }

    protected Response UnknownError( HttpServletResponse response) throws IOException {
        return respondError(500,"Unexpected Error", response);

    }

}

