package com.sample.robot.service;

import com.sample.robot.base.BaseException;
import com.sample.robot.model.entity.GameState;
import com.sample.robot.model.repo.GameStateRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.sample.robot.base.Constants.Directions.*;
import static com.sample.robot.base.Constants.GRID_LIMITS;

@Service
public class GameLogicService {
    private Logger logger = LoggerFactory.getLogger(GameLogicService.class);
    private GameStateRepo gameStateRepo;

    public GameLogicService(GameStateRepo gameStateRepo) {
        this.gameStateRepo = gameStateRepo;
    }

    public GameState runScript(String script) {
        GameState state = getState();

        switch (script) {
            case "POSITION-1":
                state.setX(0);
                state.setY(0);
                state.setD(0);
                break;
            case "WAIT":
                break;
            case "RIGHT":
                turnRight(state);
                break;
            case "TURNAROUND":
                turnAround(state);
                break;
            case "FORWARD-1":
                moveForward(state, 1);
                break;
            case "FORWARD-2":
                moveForward(state, 2);
                break;
            case "FORWARD-3":
                moveForward(state, 3);
                break;
        }

        gameStateRepo.updateMainGameState(state.getX(), state.getY(), state.getD());
        return state;
    }


    public GameState getState() {
        GameState state = gameStateRepo.getMainGameState();
        if (state == null)
            gameStateRepo.insert(new GameState(456, 0, 0, 0));
        return gameStateRepo.getMainGameState();
    }


    /*------------------------------*/
    private void moveForward(GameState state, int steps) {
        switch (state.getD()) {
            case RIGHT:
                state.setX(incrementPosition(state.getX(), steps));
                break;
            case LEFT:
                state.setX(incrementPosition(state.getX(), -1 * steps));
                break;
            case UP:
                state.setY(incrementPosition(state.getY(), -1 * steps));
                break;
            case DOWN:
                state.setY(incrementPosition(state.getY(), steps));
                break;
        }
    }

    private int incrementPosition(int before, int steps) {
        int newPosition = before + steps;
        if (newPosition < 0 || newPosition >= GRID_LIMITS)
            throw new BaseException(400, "Cannot be done!");

        return newPosition;
    }

    private void turnAround(GameState state) {
        state.setD((state.getD() + 2) % 4);
    }

    private void turnRight(GameState state) {
        state.setD((state.getD() + 1) % 4);

    }
}
