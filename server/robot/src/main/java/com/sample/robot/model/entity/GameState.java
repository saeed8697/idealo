package com.sample.robot.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameState {

    @Id
    private long id;

    private int x;

    private int y;

    /*
    * Direction
    * 0 = right
    * 1 = down
    * 2 = left
    * 3 = right
    * */
    private int d;
}
