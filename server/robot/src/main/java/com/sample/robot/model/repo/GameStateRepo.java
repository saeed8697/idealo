package com.sample.robot.model.repo;

import com.sample.robot.model.entity.GameState;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameStateRepo  {

    private MongoTemplate mongoTemplate;

    public GameStateRepo(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    //the main game's id is 456
    public GameState getMainGameState(){
        Query query = new Query(Criteria.where("_id").is(456));
        return mongoTemplate.findOne(query, GameState.class);
    }

    public void updateMainGameState(int x, int y, int d){
        Query query = new Query(Criteria.where("_id").is(456));
        mongoTemplate.updateFirst(query,
                new Update().set("x", x).set("y", y).set("d", d),
                GameState.class);
    }

    public void insert(GameState gameState){
        mongoTemplate.insert(gameState);
    }


}
