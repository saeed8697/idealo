package com.sample.robot.controller;

import com.sample.robot.base.BaseController;
import com.sample.robot.base.BaseException;
import com.sample.robot.base.Constants;
import com.sample.robot.pojo.request.RunScriptRequest;
import com.sample.robot.pojo.response.Response;
import com.sample.robot.service.GameLogicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.sample.robot.base.Constants.SCRIPTS;


@RestController
@RequestMapping("/api/v1/game")
public class GameAPIController extends BaseController {
    private Logger logger = LoggerFactory.getLogger(GameAPIController.class);
    private GameLogicService gameLogicService;

    public GameAPIController(GameLogicService gameLogicService) {
        this.gameLogicService = gameLogicService;
    }

    @PostMapping("/run")
    public Response runScript(@RequestBody RunScriptRequest req, HttpServletResponse response) throws IOException {
        //data validation
        if (req.getScript() == null || req.getScript().trim().equals(""))
            return invalidRequest(response);
        if (!SCRIPTS.contains(req.getScript()))
            return invalidRequest(response);


        try {
            return new Response<>(true, "done", gameLogicService.runScript(req.getScript().trim()));
        } catch (BaseException e) {
            return respondError(e.getErrorCode(), e.getMessage(), response);
        } catch (Exception e) {
            return UnknownError(response);
        }
    }


    @GetMapping("/state")
    public Response getGameState(HttpServletResponse response) throws IOException {

        try {
            return new Response<>(true, "done", gameLogicService.getState());
        } catch (BaseException e) {
            return respondError(e.getErrorCode(), e.getMessage(), response);
        } catch (Exception e) {
            e.printStackTrace();
            return UnknownError(response);
        }
    }
}
