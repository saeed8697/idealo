package com.sample.robot.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final List<String> SCRIPTS = Arrays.asList(
            "POSITION-1",
            "WAIT",
            "RIGHT",
            "TURNAROUND",
            "FORWARD-1",
            "FORWARD-2",
            "FORWARD-3");


    public static final int GRID_LIMITS = 5;
    public static class Directions{
        public static final int RIGHT = 0;
        public static final int DOWN = 1;
        public static final int LEFT = 2;
        public static final int UP = 3;
    }
}
