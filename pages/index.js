import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import $ from "jquery";
import {useEffect, useState} from "react";
import {getState, run, runscript, SCRIPT} from "../action";

export default function Home(props) {
    // data of position of the robot
    const [data, setdata] = useState({x: props.data.x, y: props.data.y, d: props.data.d});
    //handle loading state
    const [loading, setloading] = useState(false);
    /***
     *
     * create layer of movement
     */
    const createBoard = function () {
        const x = ["4", "3", "2", "1", "0"];
        const y = [0, 1, 2, 3, 4];


        const tile = $(".tile")
        let count = 0;


        /*Creates  white/black style .
      - For loop between 1 - 5
      - Nested another for loop 1 - 5
      */

        for (let i = 0; i < x.length; i++) {
            for (let j = 0; j < y.length; j++) {
                if (count % 5 === 0) {
                    $("#chessboard").append('<div class="tile smoothfade" style="clear:left"></div>');

                } else {
                    $("#chessboard").append('<div class="tile smoothfade"></div>');
                }
                $(".tile").eq(count).attr("data-gridpos", ((x[x.length - (i + 1)] + y[j])));

                if (((i % 2 === 0) && (j % 2 !== 0)) || ((i % 2 !== 0) && (j % 2 === 0))) {
                    $(".tile").eq(count).addClass("black");
                } else {
                    $(".tile").eq(count).addClass("white");
                }
                count++;

            }
        }
        arrangedata({x: 0, y: 0, d: 0})


    };


    /**
     *
     * call runscript api for get next move
     * @param type
     */
    const callApi = function (type) {
        if (loading) {
            alert("wait")
        }
        setloading(true)


        const script = SCRIPT[type]


        runscript({script: script}).then((forms) => {
            if (forms.success) {


                arrangedata({x: forms.data.x, y: forms.data.y, d: forms.data.d})

                setloading(false)

            } else {
                setloading(false)

            }
        }).catch(exe => {

            setloading(false)

            if (exe.response?.status === 400) {
                alert("move is not allowed ")

            } else {
                alert("failed to fetch data  " +exe)

            }

        })


    }
    /***
     *
     *  handle icon changes on grid
     * @param datainfo = {x , y , d }
     */
    const arrangedata = function (datainfo) {
        const data = datainfo.y.toString() + datainfo.x.toString();
        console.log(data)


        const player = $("#player");

        player.css({'transform': 'rotate(' + 90 * datainfo.d + 'deg)'});

        const tile = $('*[data-gridpos="' + data + '"]');

        const midY = $(tile).position().top += ($(tile).width() / 2);
        const midX = $(tile).position().left += ($(tile).width() / 2);

        console.log("X : " + midX + " || Y : " + midY);

        player.css({"top": midY - (0.5 * player.width()), "left": midX - (0.5 * player.width())});
        setdata({x: datainfo.x, y: datainfo.y, d: datainfo.d})

    }

    useEffect(() => {
        createBoard();
        arrangedata(data)
        window.addEventListener("resize", () => arrangedata(data), false);

    }, [])
    return (
        <>

            <div className="main">
                <div id="chessboard">
                    <div id="player" className="smoothmove"><span className="icon">♖ > </span></div>
                </div>
                <div>

                    <div>

                        <div className="cable"/>
                        <div className="controller">
                            <div className="centerBlue" onClick={() => {
                                callApi(0)
                            }}/>
                            <div className="centerStart" onClick={() => {
                                callApi(1)
                            }}/>


                            <div className="controllerLeft">
                                <div className="circle"/>
                                <div className="crossCenter">
                                    <div className="crossTop"/>
                                    <div className="crossBottom"/>
                                    <div className="crossLeft"/>
                                    <div className="crossRight" onClick={() => {
                                        callApi(2)
                                    }}/>
                                    <div className="crossCircle"/>
                                </div>
                            </div>
                            <div className="controllerRight">
                                <div className="backButton1Center">
                                    <div className="cornerLeft1" onClick={() => {
                                        callApi(3)
                                    }}/>
                                    <div className="cornerRight1" onClick={() => {
                                        callApi(4)
                                    }}/>
                                </div>
                                <div className="backButton2Center">
                                    <div className="cornerLeft2" onClick={() => {
                                        callApi(5)
                                    }}/>
                                    <div className="cornerRight2" onClick={() => {
                                        callApi(6)
                                    }}/>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div className="d-flex">
                    <div>
                        <h1 id="xpos">
                            {data.x}
                        </h1>

                        <h3>
                            position of x (0 - 4)
                        </h3>

                    </div>
                    <div className="mt-70">
                        <ul>
                            <li type="square" style={{color: "#0099FF"}}>{SCRIPT[0]}</li>
                            <li type="square" style={{color: "#B8B8B8"}}>{SCRIPT[1]}</li>
                            <li type="square" style={{color: "#333333"}}>{SCRIPT[2]}</li>
                            <li type="circle" style={{color: "#00B800"}}>{SCRIPT[3]}</li>
                            <li type="disc" style={{color: "#6699FF"}}>{SCRIPT[4]}</li>
                            <li type="disc" style={{color: "#CFCF00"}}>{SCRIPT[5]}</li>
                            <li type="disc" style={{color: "#7D230D"}}>{SCRIPT[6]}</li>
                        </ul>
                    </div>

                    <div className="float-right">
                        <h1 id="ypos">
                            {data.y}
                        </h1>
                        <h3>
                            position of y (0 - 4)
                        </h3>

                    </div>

                </div>

            </div>


        </>

    )
}

export async function getServerSideProps(context) {
    const state = await getState()

    return {
        props: {
            data: state.data,
        }
    }
}
