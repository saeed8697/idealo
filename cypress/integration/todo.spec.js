
describe('example to-do app', () => {
  Cypress.config('chromeWebSecurity', false);
  beforeEach(function () {
    Cypress.config('chromeWebSecurity', false);

  });
  it('should move right on initl ', () => {
    // Start from the index page
    Cypress.config('chromeWebSecurity',false);
    cy.visit('http://localhost:3000/')
    cy.get('*[class^="centerBlue"]').click()

    cy.get('*[class^="cornerRight1"]').click()
    cy.get('#xpos').should('have.text', '1')


  })
  it('should move bottom on initial ', () => {
    // Start from the index page
    cy.get('*[class^="centerBlue"]').click()
    cy.get('*[class^="crossRight"]').click()

    cy.get('*[class^="cornerRight1"]').click()
    cy.get('#ypos').should('have.text', '1')


  })
})
